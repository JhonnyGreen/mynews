//
//  News+CoreDataClass.swift
//  News
//
//  Created by Jhonny Green on 13.04.2020.
//  Copyright © 2020 Test Lessons. All rights reserved.
//
//

import Foundation
import CoreData

@objc(News)
public class News: NSManagedObject {

    enum CodingKeys: String, CodingKey {
           case url
           case source
           case publishedDate = "published_date"
           case byline, type, title, abstract
           case media
       }

    
}
