//
//  News+CoreDataProperties.swift
//  News
//
//  Created by Jhonny Green on 13.04.2020.
//  Copyright © 2020 Test Lessons. All rights reserved.
//
//

import Foundation
import CoreData


extension News {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<News> {
        return NSFetchRequest<News>(entityName: "News")
    }

    @NSManaged public var image: Data?
    @NSManaged public var newsDescription: String?
    @NSManaged public var newsType: String?
    @NSManaged public var postedDate: String?
    @NSManaged public var source: String?

}
