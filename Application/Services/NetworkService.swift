//
//  NetworkService.swift
//  News
//
//  Created by Jhonny Green on 24.02.2020.
//  Copyright © 2020 Test Lessons. All rights reserved.
//

import Foundation
import Alamofire
import CoreData


class NetworkService {
    
    lazy var persistantContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "News")
        container.loadPersistentStores { store, error in
            
        }
        return container
    }()
    
    static func fetchNews(url: String, completion: @escaping (_ newsFromIntermet: [Result]) -> ()) {
        
        guard let url = URL(string: url) else { return }
        
        
        
        AF.request(url, method: .get).validate().responseData { response in
            
            switch response.result {
            case .success(let data):
                let decoder = JSONDecoder()
                if let newsResponse = try? decoder.decode(Empty.self, from: data) {
                    
                    let newses = newsResponse.results
                    completion(newses)
                    
                    
               
                } else {
                    print("Invalid response data in news")
                }
                
                
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
}

