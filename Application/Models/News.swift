//
//  News.swift
//  News
//
//  Created by Jhonny Green on 25.02.2020.
//  Copyright © 2020 Test Lessons. All rights reserved.
//

import Foundation



// MARK: - Empty
struct Empty: Decodable {
    let copyright: String
    let results: [Result]
    
    enum CodingKeys: String, CodingKey {
        case copyright
        case results
    }
//    init(from decoder: Decoder) throws {
//        let container = try decoder.container(keyedBy: CodingKeys.self)
//        self.copyright = try container.decode(String.self, forKey: .copyright)
//        self.results = try container.decode([Result].self, forKey: .results)
//        
//    }
}

// MARK: - Result
struct Result: Decodable {
    let url: String
    let source: String
    let publishedDate: String
    let byline: String
    let type: String
    let title, abstract: String
    let media: [Media]
    
    enum CodingKeys: String, CodingKey {
        case url
        case source
        case publishedDate = "published_date"
        case byline, type, title, abstract
        case media
    }
//    init(from decoder: Decoder) throws {
//        let container = try decoder.container(keyedBy: CodingKeys.self)
//        self.url = try container.decode(String.self, forKey: .url)
//        self.source = try container.decode(String.self, forKey: .source)
//        self.publishedDate = try container.decode(String.self, forKey: .publishedDate)
//        self.byline = try container.decode(String.self, forKey: .byline)
//        self.type = try container.decode(String.self, forKey: .type)
//        self.title = try container.decode(String.self, forKey: .title)
//        self.abstract = try container.decode(String.self, forKey: .abstract)
//        self.media = try container.decode([Media].self, forKey: .media)
//        
//    }
}

// MARK: - Media
struct Media: Decodable {
    let caption, copyright: String
    let mediaMetadata: [MediaMetadatum]
    
    enum CodingKeys: String, CodingKey {
        case caption, copyright
        case mediaMetadata = "media-metadata"
    }
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.copyright = try container.decode(String.self, forKey: .copyright)
        self.caption = try container.decode(String.self, forKey: .caption)
        self.mediaMetadata = try container.decode([MediaMetadatum].self, forKey: .mediaMetadata)
        
    }
}

// MARK: - MediaMetadatum
struct MediaMetadatum: Decodable {
    let url: String

    
    enum CodingKeys: String, CodingKey {
        case url

    }
//    init(from decoder: Decoder) throws {
//        let container = try decoder.container(keyedBy: CodingKeys.self)
//        self.url = try container.decode(String.self, forKey: .url)
//    }
}
