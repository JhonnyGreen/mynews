//
//  MostEmailedWebViewController.swift
//  News
//
//  Created by Jhonny Green on 26.02.2020.
//  Copyright © 2020 Test Lessons. All rights reserved.
//

import UIKit
import WebKit

class MostEmailedWebViewController: UIViewController, WKNavigationDelegate {
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var mostEmailedWebView: WKWebView!
    
    var selectedNews: String?
    var newsURL = ""
    
    
    override func viewDidLoad() {
            super.viewDidLoad()
            
            title = selectedNews
            
            guard let url = URL(string: newsURL) else { return }
            let request = URLRequest(url: url)
        
            mostEmailedWebView.load(request)
            mostEmailedWebView.allowsBackForwardNavigationGestures = true
            mostEmailedWebView.navigationDelegate = self
            activityIndicator.stopAnimating()
            
        }
    }

