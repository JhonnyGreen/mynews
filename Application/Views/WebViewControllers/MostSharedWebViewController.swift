//
//  MostSharedWebViewController.swift
//  News
//
//  Created by Jhonny Green on 09.04.2020.
//  Copyright © 2020 Test Lessons. All rights reserved.
//

import UIKit
import WebKit

class MostSharedWebViewController: UIViewController, WKNavigationDelegate {
    
    @IBOutlet weak var mostSharedWebView: WKWebView!
    var selectedNews: String?
    var newsURL = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = selectedNews
        
        guard let url = URL(string: newsURL) else { return }
        let request = URLRequest(url: url)
        
        mostSharedWebView.load(request)
        mostSharedWebView.allowsBackForwardNavigationGestures = true
        mostSharedWebView.navigationDelegate = self
        
        
    }
}

