//
//  MostViewedTableViewCell.swift
//  News
//
//  Created by Jhonny Green on 24.02.2020.
//  Copyright © 2020 Test Lessons. All rights reserved.
//

import UIKit

protocol ButtonInViewedTableView{
    func saveClickCell(index: Int)
}

class MostViewedTableViewCell: UITableViewCell {
    
    var cellDelegate: ButtonInViewedTableView?
    var index: IndexPath?

    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var newsDescriptionLabel: UILabel!
    @IBOutlet weak var newsTypeLabel: UILabel!
    @IBOutlet weak var postedDateLabel: UILabel!
    @IBOutlet weak var sourceLabel: UILabel!
    
    @IBAction func saveStarButton(_ sender: Any) {
        cellDelegate?.saveClickCell(index: index!.row)
    }
    

}
