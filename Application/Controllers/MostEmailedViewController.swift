//
//  MostEmailedViewController.swift
//  News
//
//  Created by Jhonny Green on 24.02.2020.
//  Copyright © 2020 Test Lessons. All rights reserved.
//

import UIKit
import AlamofireImage
import Alamofire
import CoreData

class MostEmailedViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    
    
    @IBOutlet weak var mostEmailedTableView: UITableView!
    var newsArray = [Result]()
    var newsToSave: [NSManagedObject] = []
    private var nameOfNews: String?
    private var newsUrl: String?
    var refreshControl = UIRefreshControl()

    
    
    private let url = "https://api.nytimes.com/svc/mostpopular/v2/emailed/7.json?api-key=eaGWmuNSRjeON3SzZb0xoh5lORLrLE7H"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchMostEmailedNewsWithALamofire()
        addRefreshControl()
        
    }
    func addRefreshControl() {
        refreshControl.tintColor = UIColor.green
        refreshControl.addTarget(self, action: #selector(refreshContents), for: .valueChanged)
        if #available(iOS 10.0, *) {
            mostEmailedTableView.refreshControl = refreshControl
        } else {
            mostEmailedTableView.addSubview(refreshControl)
        }
    }
    @objc func refreshContents() {
        self.perform(#selector(finishedRefreshing), with: nil, afterDelay: 1.0)
    }
    @objc func finishedRefreshing() {
        refreshControl.endRefreshing()
    }

    
    
    func fetchMostEmailedNewsWithALamofire() {
        NetworkService.fetchNews(url: url) { news in
            self.newsArray = news
            DispatchQueue.main.async {
                self.mostEmailedTableView.reloadData()
            }
        }
    }
    private func configureCell(cell: MostEmailedTableViewCell, for indexPath: IndexPath) {
        let news = newsArray[indexPath.row]
        cell.newsDescriptionLabel.text = news.abstract
        cell.newsTypeLabel.text = news.title
        cell.postedDateLabel.text = news.publishedDate
        cell.sourceLabel.text = news.source
        
        guard let path = newsArray[indexPath.row].media.first?.mediaMetadata.first?.url,
        let url = URL(string: path) else { return }
        AF.request(url).responseData { [cell] result in
            guard let imageData = result.data else {
                print("error .....")
                return
            }
            let image = UIImage(data: imageData)
            let size = CGSize(width: 110.0, height: 110.0)
            let scaledImage = image?.af.imageScaled(to: size)
            let roundedImage = scaledImage?.af.imageRounded(withCornerRadius: 10.0)
            DispatchQueue.main.async {
                cell.imageView?.image = roundedImage
                self.mostEmailedTableView.reloadData()
            }
        }
    }
    
    // MARK: - Table View Data Source
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return newsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "EmailedCell", for: indexPath) as! MostEmailedTableViewCell
        
        configureCell(cell: cell, for: indexPath)
        cell.cellDelegate = self
        cell.index = indexPath
        
        return cell
    }
    
    // MARK: - Table View Delegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let news = newsArray[indexPath.row]
        
        newsUrl = news.url
        nameOfNews = news.title
        
        performSegue(withIdentifier: "MostEmailedWebViewSegue", sender: self)

        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let webViewController = segue.destination as! MostEmailedWebViewController
        webViewController.selectedNews = nameOfNews
        if let url = newsUrl {
            webViewController.newsURL = url
        }
    }
}

extension MostEmailedViewController: ButtonInEmailedTableView {
    func saveClickCell(index: Int) {
        print("\(newsArray[index]) is selected" )
    }
    
    
}

    
    

