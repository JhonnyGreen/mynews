//
//  MostViewedViewController.swift
//  News
//
//  Created by Jhonny Green on 24.02.2020.
//  Copyright © 2020 Test Lessons. All rights reserved.
//

import UIKit
import AlamofireImage
import Alamofire

class MostViewedViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var mostViewedTableView: UITableView!
    private var nameOfNews: String?
    private var newsUrl: String?
    
    private var newsArray = [Result]()
    private let url = "https://api.nytimes.com/svc/mostpopular/v2/viewed/1.json?api-key=eaGWmuNSRjeON3SzZb0xoh5lORLrLE7H"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        fetchMostViewedNewsWithALamofire()
    }
    func fetchMostViewedNewsWithALamofire() {
        NetworkService.fetchNews(url: url) { (newsFromIntermet) in
            
            self.newsArray = newsFromIntermet
            DispatchQueue.main.async {
                self.mostViewedTableView.reloadData()
            }
        }
    }
    
    private func configureCell(cell: MostViewedTableViewCell, for indexPath: IndexPath) {
        
        let news = newsArray[indexPath.row]
        cell.newsDescriptionLabel.text = news.title
        cell.newsTypeLabel.text = news.abstract
        cell.postedDateLabel.text = news.publishedDate
        cell.sourceLabel.text = news.source
        
        guard let path = newsArray[indexPath.row].media.first?.mediaMetadata.first?.url,
            let url = URL(string: path) else { return }
        AF.request(url).responseData { [cell] result in
            guard let imageData = result.data else {
                print("error .....")
                return
            }
            let image = UIImage(data: imageData)
            let size = CGSize(width: 110.0, height: 110.0)
            let scaledImage = image?.af.imageScaled(to: size)
            let roundedImage = scaledImage?.af.imageRounded(withCornerRadius: 10.0)
            DispatchQueue.main.async {
                cell.imageView?.image = roundedImage
                self.mostViewedTableView.reloadData()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return newsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ViewedCell", for: indexPath) as! MostViewedTableViewCell
        configureCell(cell: cell, for: indexPath)
        cell.cellDelegate = self
        cell.index = indexPath
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 180
    }
    
    
    
    // MARK: - Table View Delegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let news = newsArray[indexPath.row]
        
        newsUrl = news.url
        nameOfNews = news.title
        
        performSegue(withIdentifier: "MostViewedWebViewSegue", sender: self)
        
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let webViewController = segue.destination as! MostViewedWebViewController
        webViewController.selectedNews = nameOfNews
        if let url = newsUrl {
            webViewController.newsURL = url
        }
    }
}

extension MostViewedViewController: ButtonInViewedTableView {
    func saveClickCell(index: Int) {
        print("\(newsArray[index]) is selected" )
    }
    
    
}
