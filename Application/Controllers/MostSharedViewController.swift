//
//  MostSharedViewController.swift
//  News
//
//  Created by Jhonny Green on 24.02.2020.
//  Copyright © 2020 Test Lessons. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class MostSharedViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var mostSharedTableView: UITableView!
    private var nameOfNews: String?
    private var newsUrl: String?
    
    private var newsArray = [Result]()
    private let url = "https://api.nytimes.com/svc/mostpopular/v2/shared/1/facebook.json?api-key=eaGWmuNSRjeON3SzZb0xoh5lORLrLE7H"
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchMostSharedNewsWithALamofire()
        
    }
    func fetchMostSharedNewsWithALamofire() {
        NetworkService.fetchNews(url: url) { news in
            self.newsArray = news
            DispatchQueue.main.async {
                self.mostSharedTableView.reloadData()
            }
        }
    }
    private func configureCell(cell: MostSharedTableViewCell, for indexPath: IndexPath) {
        
        let news = newsArray[indexPath.row]
        cell.newsDescriptionLabel.text = news.title
        cell.newsTypeLabel.text = news.abstract
        cell.postedDateLabel.text = news.publishedDate
        cell.sourceLabel.text = news.source
        
        guard let path = newsArray[indexPath.row].media.first?.mediaMetadata.first?.url,
        let url = URL(string: path) else { return }
        AF.request(url).responseData { [cell] result in
            guard let imageData = result.data else {
                print("error .....")
                return
            }
            let image = UIImage(data: imageData)
            let size = CGSize(width: 110.0, height: 110.0)
            let scaledImage = image?.af.imageScaled(to: size)
            let roundedImage = scaledImage?.af.imageRounded(withCornerRadius: 10.0)
            DispatchQueue.main.async {
                cell.imageView?.image = roundedImage
                self.mostSharedTableView.reloadData()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return newsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SharedCell", for: indexPath) as! MostSharedTableViewCell
        configureCell(cell: cell, for: indexPath)
        
        cell.cellDelegate = self
        cell.index = indexPath
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 180
    }
    
    
    
     // MARK: - Table View Delegate
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            let news = newsArray[indexPath.row]
            
            newsUrl = news.url
            nameOfNews = news.title
            
            performSegue(withIdentifier: "MostSharedWebViewSegue", sender: self)

            
        }
        override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
            let webViewController = segue.destination as! MostSharedWebViewController
            webViewController.selectedNews = nameOfNews
            if let url = newsUrl {
                webViewController.newsURL = url
            }
        }
    }

extension MostSharedViewController: ButtonInSharedTableView {
    func saveClickCell(index: Int) {
        print("\(newsArray[index]) is selected" )
    }
    
    
}
